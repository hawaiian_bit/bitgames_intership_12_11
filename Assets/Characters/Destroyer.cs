﻿using System;
using UnityEngine;

public class Destroyer : MonoBehaviour
{

    [SerializeField] private float high;

    private void OnBecameInvisible()
    {
        if (transform.position.y <= high)
        {
            Destroy(gameObject);
        }
    }
}
