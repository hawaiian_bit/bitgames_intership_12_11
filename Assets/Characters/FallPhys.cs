﻿using UnityEngine;

public class FallPhys : MonoBehaviour
{
    [SerializeField] private Rigidbody rig;
    [SerializeField] private Vector3 force;
    [SerializeField] private Vector3 torque;

    private void Start()
    {
        rig.gameObject.transform.SetParent(null);
        rig.useGravity = true;
        rig.AddForce(force);
        rig.AddTorque(torque);
    }
}
