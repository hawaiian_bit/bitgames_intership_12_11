﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Fireball : MonoBehaviour
{
    enum State 
    {
        Casting,
        Shooting
    }

    [Header("Casting")]
    [SerializeField] private Transform lHand;
    [SerializeField] private Transform rHand;

    [Header("Shooting")]
    [SerializeField] private Transform target;
    [SerializeField] private float speed = 1.0f;

    [Header("Hitting")]
    [SerializeField] private GameObject hitEffect;

    private State state = State.Casting;

    private Vector3 direction;

    public void Shoot()
    {
        state = State.Shooting;
        direction = (target.position - transform.position).normalized;        
    } 

    private void Update()
    {
        switch (state)
        {
            case State.Casting:
                transform.position = Vector3.Lerp(lHand.position, rHand.position, 0.5f);
                break;
            case State.Shooting:
                transform.position += direction * (speed * Time.deltaTime);
                break;
        }
    }      

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (hitEffect != null) 
            {
                Instantiate(hitEffect, collision.GetContact(0).point, Quaternion.identity);
            }

            Destroy(gameObject);
        }       
    }
}
