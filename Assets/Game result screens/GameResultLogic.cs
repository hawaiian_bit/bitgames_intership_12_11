﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameResultLogic : MonoBehaviour
{

    [SerializeField] private Image darkScreen;
    [SerializeField] [Range(0, 1)] private float darkSpeed;


    private void Start()
    {
        StartCoroutine(LighterScreen());
    }


    private IEnumerator LighterScreen()
    {
        while (darkScreen.color.a > 0)
        {
            var transparency = darkScreen.color.a;
            transparency -= darkSpeed * Time.deltaTime;
            darkScreen.color = new Color(0,0,0, transparency);

            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        yield return StartCoroutine(LighterTxt());
    }

    private IEnumerator ReloadGame()
    {
        inscription.gameObject.SetActive(false);
        
        while (darkScreen.color.a < 1)
        {
            var transparency = darkScreen.color.a;
            transparency += darkSpeed * Time.deltaTime;
            darkScreen.color = new Color(0,0,0, transparency);

            yield return new WaitForSeconds(Time.deltaTime);
        }

        SceneManager.LoadScene("GameScene");
        
        yield return null;
    }


    [SerializeField] private Text inscription;
    
    private IEnumerator LighterTxt()
    {
        var color = inscription.color;
        
        while (color.a < 1)
        {
            color.a = inscription.color.a;
            color.a += darkSpeed * Time.deltaTime;

            inscription.color = color;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        yield return StartCoroutine(DarkerTxt());
    }

    private IEnumerator DarkerTxt()
    {
        var color = inscription.color;
        while (color.a > 0)
        {
            color.a = inscription.color.a;
            color.a -= darkSpeed * Time.deltaTime;

            inscription.color = color;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        yield return StartCoroutine(LighterTxt());
    }


    private bool _is1stTime = true;
    void Update()
    {
        if (_is1stTime && Input.anyKey)
        {
            StopAllCoroutines();
            
            _is1stTime = false;
            
            StartCoroutine(ReloadGame());
        }
    }
}
