﻿using System;
using System.Collections;  
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField] private InstTargets instTargets;
    [SerializeField] private Gun gun;
    [SerializeField] private UI ui;

    private bool _checkpointActive;

    [NonSerialized] public int hitCounter;
    [NonSerialized] public int stepCounter;
    [NonSerialized] public int globalHitCounter;
    

    public int possibleMissCount;
    public int hitCount;


    public PlayableDirector playableDirector;

    [SerializeField] private Camera gameplayCamera;

    private float checkpointNum;

    [NonSerialized] public bool inGame;

    [SerializeField] private Image darkScreen;
    [SerializeField] [Range(0, 1)] private float darkSpeed;

    public void Start()
    {
        StartCoroutine(UnDarkScreen());
        playableDirector.Pause();
    }

    private IEnumerator UnDarkScreen()
    {
        while (darkScreen.color.a > 0)
        {
            var transparency = darkScreen.color.a;
            transparency -= darkSpeed * Time.deltaTime;
            darkScreen.color = new Color(0,0,0,transparency);

            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        yield return null;
    }

    public void StartCheckpoint(int targetsCount, Color targetsColor, float gunSpeed, int targetModelIndex)
    {
        hitCount = targetsCount;
        hitCounter = 0;

        gun.speedX = gunSpeed;
        
        instTargets.modelIndex = targetModelIndex;
        instTargets.Reload(targetsColor);
        
        ui.TargetColorChanger(targetsColor);
        ui.ResetTargetMonitoringScale();

        checkpointNum = targetModelIndex;

        _checkpointActive = true;

        StartCoroutine(ShowGameplay(1));
    }

    public void CheckGameStatus()
    {
        if (!_checkpointActive) return;

        if (!inGame)
        {
            stepCounter = 0;
            if (hitCounter >= 1)
            {
                inGame = true;
                CheckpointVictory();
            }
            hitCounter = 0;
            globalHitCounter = 0;
            
            return;
        }

        if (hitCounter >= hitCount)
        {
            CheckpointVictory(); 
        }

        if (stepCounter - globalHitCounter >= possibleMissCount)
        {
            StartCoroutine(Defeat());
        }

        ui.TargetMonitoringScale(hitCounter, hitCount);
        ui.HeroHPMonitoringScale(stepCounter - globalHitCounter, possibleMissCount);
    }

    public IEnumerator HideGameplay(float speed)
    {
        while (gameplayCamera.rect.height > 0)
        {
            var rectHeight = gameplayCamera.rect.height - speed * Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
            
            gameplayCamera.rect = new Rect (0, 0, 1, rectHeight);
        }
        
        gameplayCamera.gameObject.SetActive(false);
        gun.enabled = false;
        
        //TODO: transfer at the end of the animation 
        ui.EnemyHPMonitoringScale(checkpointNum,4);
        
        yield return null;
    }

    public IEnumerator ShowGameplay(float speed)
    {
        gun.enabled = true;
        
        gameplayCamera.gameObject.SetActive(true);
        
        while (gameplayCamera.rect.height < 0.3f)
        {
            var rectHeight = gameplayCamera.rect.height + speed * Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
            
            gameplayCamera.rect = new Rect (0, 0, 1, rectHeight);
        }

        gun.isCheckpoint = true;
        
        yield return null;
    }
    
    private IEnumerator Defeat()
    {

        StartCoroutine(HideGameplay(1));

        while (darkScreen.color.a < 1)
        {
            var transparency = darkScreen.color.a;
            transparency += darkSpeed * Time.deltaTime;
            darkScreen.color = new Color(0,0,0,transparency);

            yield return new WaitForSeconds(Time.deltaTime);
        }

        SceneManager.LoadScene("Defeat");
        
        yield return null;
    }
    
    

    private void CheckpointVictory()
    {
        StartCoroutine(HideGameplay(1));
        playableDirector.Resume();
        _checkpointActive = false;
    }
    
}
