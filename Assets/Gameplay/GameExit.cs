﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameExit : MonoBehaviour
{

    [SerializeField] private Game game;
    [SerializeField] private Gun gun;
    [SerializeField] private IntroLogic introLogic;
    [SerializeField] private GameObject introUI;
    [Space]
    [SerializeField] private GameObject exitWindow;
    [SerializeField] private Button buttonYes, buttonNo;

    private void Start()
    {
        exitWindow.SetActive(false);
        
        buttonYes.onClick.AddListener(OnClickYes);
        buttonNo.onClick.AddListener(OnClickNo);
    }

    private void OnGUI()
    {
        if (!Input.GetKeyDown(KeyCode.Escape)) return;

        if (game.inGame || gun.mainButton != KeyCode.None)
        {
            SceneManager.LoadScene("GameScene");
        }
        else
        {
            if (introLogic)
                introLogic.enabled = false;
            if(introUI)
                introUI.SetActive(false);
            exitWindow.SetActive(true);
            gun.isCheckpoint = false;
        }
    }

    public void OnClickYes()
    {
        Application.Quit();
        Debug.Log("Exit!");
    }

    public void OnClickNo()
    {
        exitWindow.SetActive(false);
        gun.isCheckpoint = true;
        if (introLogic)
            introLogic.enabled = true;
        if(introUI)
            introUI.SetActive(true);
    }
}
