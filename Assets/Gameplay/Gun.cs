﻿using System;
using System.Collections;
using UnityEngine;

public class Gun : MonoBehaviour
{

    public Game game;
    public InstTargets instTargets;
    
    public float speedX;
    public float speedY;

    [SerializeField] private AudioSource hitSound, missSound;

    [NonSerialized]
    public KeyCode mainButton = KeyCode.None;

    [NonSerialized] public bool isCheckpoint;
    
    private void Update()
    {
        if (isCheckpoint && Input.GetKeyDown(mainButton))
        {
            StartCoroutine(nameof(ShootAnimation));
            
            var ray = new Ray(transform.position, Vector3.forward);

            Physics.Raycast(ray, out var hit);
            
            Shoot(hit.collider);
            
            game.stepCounter++;
            
            game.CheckGameStatus();
        }
        else
        {
            Motion();
        }
        
    }
    

    #region ShootAnimations

    
    [SerializeField] private int addedTwists = 2;
    
    private IEnumerator ShootDownTargetAnimation(GameObject target)
    {
        target.GetComponent<TargetRotation>().speed += 360 * addedTwists;
        
        yield return new WaitForSeconds(1);
        
        target.GetComponent<TargetRotation>().speed -= 360 * addedTwists;

        yield return null;
    }
    

    [SerializeField] private float maxHeight = -2f;
    [SerializeField] private float minHeight = -2.5f;
    
    private IEnumerator ShootAnimation()
    {
        var yPos = transform.position.y;
        
        while (yPos < maxHeight)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            
            yPos += speedY * Time.deltaTime;

            var transform1 = transform;
            var position = transform1.position;
            position = new Vector3(position.x, yPos, position.z);
            transform1.position = position;
        }
        
        while (yPos > minHeight)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            
            yPos -= speedY * Time.deltaTime;

            var transform1 = transform;
            var position = transform1.position;
            position = new Vector3(position.x, yPos, position.z);
            transform1.position = position;
        }

        yield return null;
    }    

    #endregion

    private void Shoot(Collider collider)
    {
        if (collider)
        {
            var target = collider.gameObject.GetComponent<IsTarget>();
                
            if (target && target.isTarget)
            {
                hitSound.Play(0);
                    
                StartCoroutine(ShootDownTargetAnimation(target.gameObject));
                game.hitCounter++;
                game.globalHitCounter++;
                instTargets.DeactivateTarget(collider.gameObject);
            }
            else
            {
                missSound.Play(0);
            }
        }
        else
        {
            missSound.Play(0);
        }
    }


    #region Motion
    
    private float conditionalPos;
    [SerializeField] private float trackLength = 48;
    
    private void Awake()
    {
        transform.position = new Vector3(trackLength / 2, transform.position.y, transform.position.z);
    }

    private void Motion()
    {

        conditionalPos += Time.deltaTime * speedX;

        //in order not to exceed the float limit
        if (conditionalPos > Mathf.PI * 2)
        {
            conditionalPos -= Mathf.PI * 2;
        }

        var xPos = trackLength / 2 + Mathf.Sin(conditionalPos) * trackLength / 2;
        transform.position = new Vector3(xPos, transform.position.y, transform.position.z);
    }

    #endregion

}