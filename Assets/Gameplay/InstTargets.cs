﻿using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class InstTargets : MonoBehaviour
{
    //TODO: very scary & difficult script, needing something do
    
    public int targetQuantity;
    public float distance;
    public float startPos;
    
    public Color defaultColor;
    [NonSerialized] private Color rightTargetColor;
    
    public GameObject target;
    
    private GameObject[] targets;
    private int currentActiveIndex;
    private int oldCurrentActiveIndex = -1;

    [NonSerialized] public int modelIndex;

    private void Awake()
    {
        targets = new GameObject[targetQuantity];
        currentActiveIndex = Random.Range(0, targetQuantity);
    }

    private void ActivateTarget()
    {
        while (currentActiveIndex == oldCurrentActiveIndex)
        {
            currentActiveIndex = Random.Range(0, targetQuantity);
        }
        oldCurrentActiveIndex = currentActiveIndex;

        targets[currentActiveIndex].GetComponent<IsTarget>().isTarget = true;
        
        var targetModel = targets[currentActiveIndex].transform.GetChild(modelIndex).GetChild(0).gameObject;
        targetModel.GetComponent<Renderer>().material.color = rightTargetColor;
    }
    
    public void Reload(Color targetColor)
    {
        rightTargetColor = targetColor;
        
        for (var i = 0; i < targetQuantity; i++)
        {
            var targetPos = (i + 1 + startPos) * distance * Vector3.right;
            targetPos.z = 15;
            targetPos.y = 100;

            Destroy(targets[i]);
            targets[i] = Instantiate(target, targetPos, Quaternion.identity);
            
            var targetModel = targets[i].transform.GetChild(modelIndex).GetChild(0).gameObject;
            targetModel.SetActive(true);
            targetModel.GetComponent<Renderer>().material.color = defaultColor;
        }
        
        ActivateTarget();
    }

    public void DeactivateTarget(GameObject targetHit)
    {
        targetHit.GetComponent<IsTarget>().isTarget = false;
        targetHit.transform.GetChild(modelIndex).GetChild(0).GetComponent<Renderer>().material.color = defaultColor;
        ActivateTarget();
    }

}
