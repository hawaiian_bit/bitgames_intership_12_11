﻿using UnityEngine;

public class IntroLogic : MonoBehaviour
{

    [SerializeField] private Game game;
    [SerializeField] private Gun gun;
    [SerializeField] private UI ui;

    private KeyCode button;
    private string buttonName;

    private bool isSet;
    private void OnGUI()
    {
        if (!isSet && button != KeyCode.None)
        {
            gun.mainButton = button;
            game.StartCheckpoint(1, Color.white, 1, 0);
            
            ui.ShowButton(buttonName);

            isSet = true;
            
            Destroy(this);
        }
        
        var evnt = Event.current;
        
        if (evnt.isKey)
        {
            if(evnt.keyCode == KeyCode.Escape)
                return;
            
            button = evnt.keyCode;
            buttonName = button.ToString();
        }
        else if(evnt.isMouse)
        {
            switch (evnt.button)
            {
                case 0:
                    button = KeyCode.Mouse0;
                    buttonName = "LMB";
                    break;
                case 1:
                    button = KeyCode.Mouse1;
                    buttonName = "RMB";
                    break;
                case 2:
                    button = KeyCode.Mouse2;
                    buttonName = "MMB";
                    break;
                case 3:
                    button = KeyCode.Mouse3;
                    buttonName = button.ToString();
                    break;
                case 4:
                    button = KeyCode.Mouse4; 
                    buttonName = button.ToString();
                    break;
                case 5:
                    button = KeyCode.Mouse5; 
                    buttonName = button.ToString();
                    break;
                case 6:
                    button = KeyCode.Mouse6; 
                    buttonName = button.ToString();
                    break;
            }
        }
    }
}
