﻿using UnityEngine;

public class KeyboardExit : MonoBehaviour
{
    [SerializeField] private GameExit exit;

    private void OnGUI()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            exit.OnClickNo();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            exit.OnClickYes();
        }
    }
}
