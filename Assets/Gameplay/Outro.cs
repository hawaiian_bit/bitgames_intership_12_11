﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Outro : MonoBehaviour
{
    [SerializeField] private Image darkScreen;
    [SerializeField] [Range(0, 1)] private float darkSpeed;

    [SerializeField] private AudioSource finalSound;
    [SerializeField] private float volumeSpeedChanging;

    private IEnumerator Start()
    {
        while (darkScreen.color.a < 1)
        {
            var transparency = darkScreen.color.a;
            transparency += darkSpeed * Time.deltaTime;
            darkScreen.color = new Color(0,0,0,transparency);

            finalSound.volume -= Time.deltaTime * volumeSpeedChanging;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        SceneManager.LoadScene("Victory");
        
        yield return null;
    }
    
}
