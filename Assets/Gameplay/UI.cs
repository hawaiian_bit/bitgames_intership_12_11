﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    #region Intro

    [SerializeField] private Text rulesTxt;
    
    [SerializeField] private string rule1;
    [SerializeField] private string rule2;

    private void Start()
    {
        rulesTxt.text = rule1;

        StartCoroutine(Transparencing());
    }

    public void ShowButton(string button)
    {
        rulesTxt.text = button;
        
        StartCoroutine(Scaling());
    }
    
    private IEnumerator Transparencing()
    {
        var transparency = rulesTxt.color.a;

        while (transparency < 255)
        {
            transparency += Time.deltaTime;
            var color = rulesTxt.color;
            color.a = transparency;
            rulesTxt.color = color;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        yield return null;
    }

    private IEnumerator Scaling()
    {
        while (rulesTxt.transform.localScale.x < 0.5f)
        {
            var size = rulesTxt.transform.localScale.x;

            size += Time.deltaTime * 2;
            
            rulesTxt.transform.localScale = Vector3.one * size;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        while (rulesTxt.transform.localScale.x > 0)
        {
            var size = rulesTxt.transform.localScale.x;

            size -= Time.deltaTime * 2;
            
            rulesTxt.transform.localScale = Vector3.one * size;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        rulesTxt.transform.localScale = Vector3.zero;

        yield return ShowRule2();
    }

    private IEnumerator ShowRule2()
    {
        var color = rulesTxt.color;
        color.a = 0;
        rulesTxt.color = color;

        rulesTxt.text = rule2;
        
        rulesTxt.transform.localScale = Vector3.one * 0.08f;
        
        yield return Transparencing();
    }

    #endregion
    
    [Space]
    
    #region Target

    [SerializeField] private GameObject targetCounter;

    public void TargetMonitoringScale(float hitCounter, float hitCount)
    {
        var percent = 1 - hitCounter / hitCount;

        StartCoroutine(ChangingScaleLength(targetCounter.GetComponent<RectTransform>(), percent));
    }

    public void ResetTargetMonitoringScale()
    {
        var scale = targetCounter.GetComponent<RectTransform>();

        var localScale = scale.localScale;
        localScale = new Vector3(1, localScale.y, localScale.z);
        scale.localScale = localScale;
    }

    public void TargetColorChanger(Color color)
    {
        targetCounter.GetComponent<Image>().color = color;
    }

    #endregion
    
    [Space]

    #region HP

    [SerializeField] private GameObject heroHP;
    [SerializeField] private GameObject enemyHP;

    [SerializeField] private RawImage bloodyScreen;
    [Range(0,1)]
    [SerializeField] private float transparencyFactor = 1;

    [SerializeField] private float transparencySpeed = 1;
    private float transparency;

    public void HeroHPMonitoringScale(float stepCounter, float stepCount)
    {
        var percent = 1 - stepCounter / stepCount;
        
        if(percent < 0.5f)
        {
            transparency = (1 - percent) * transparencyFactor;
        }

        StartCoroutine(BloodyScreenTransparencySet(transparency));
        
        StartCoroutine(ChangingScaleLength(heroHP.GetComponent<RectTransform>(), percent));
    }

    private IEnumerator BloodyScreenTransparencySet(float transp)
    {
        var color = bloodyScreen.color;
        var transparencyNow = color.a;

        while (transparencyNow < transp)
        {
            transparencyNow += Time.deltaTime * transparencySpeed;
            color.a = transparencyNow;
            bloodyScreen.color = color;
            
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public void EnemyHPMonitoringScale(float thisCheckpointNum, float lastCheckpointNum)
    {
        var percent = 1 - thisCheckpointNum / lastCheckpointNum;
        StartCoroutine(ChangingScaleLength(enemyHP.GetComponent<RectTransform>(), percent));
    }

    #endregion
    
    [Space]

    [SerializeField] private float speed = 1;

    IEnumerator ChangingScaleLength(RectTransform scale, float percent)
    {
        var localScale = scale.localScale;
        var tempPercent = localScale.x;

        while (localScale.x > percent)
        {
            tempPercent -= Time.deltaTime / speed;

            localScale = new Vector3(tempPercent, localScale.y, localScale.z);
            scale.localScale = localScale;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        yield return null;
    }

}

