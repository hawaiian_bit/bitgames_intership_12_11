using System;
using UnityEngine;
using UnityEngine.Playables;

namespace DefaultNamespace
{
    [Serializable]
    public class CheckpointBehaviour : PlayableBehaviour
    {
        [SerializeField] private int targetsCount = 3;
        [SerializeField] private Color targetColor = Color.yellow;
        [SerializeField] private float gunSpeed = 10;
        
        [Range(0,4)]
        [SerializeField] private int targetModelIndex;

        private bool clipPlayed;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (clipPlayed || info.weight < 1f) return;

            clipPlayed = true;
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (clipPlayed)
            {
                Checkpoint(playable.GetGraph().GetResolver() as PlayableDirector);
            }

            clipPlayed = false;
        }

        private void Checkpoint(PlayableDirector director)
        {
            director.Pause();

            var game = GameObject.FindWithTag("GameController").GetComponent<Game>();
            game.StartCheckpoint(targetsCount, targetColor, gunSpeed, targetModelIndex);
        }
    }
}