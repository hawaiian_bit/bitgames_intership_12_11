using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DefaultNamespace
{
    [Serializable]
    public class CheckpointClip : PlayableAsset, ITimelineClipAsset
    {
        public CheckpointBehaviour template = new CheckpointBehaviour();
        
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<CheckpointBehaviour>.Create(graph, template);
            return playable;
        }

        public ClipCaps clipCaps => ClipCaps.None;
    }
}