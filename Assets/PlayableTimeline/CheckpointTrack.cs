using UnityEngine.Timeline;

namespace DefaultNamespace
{
    [TrackColor(0.855f, 0.903f, 0.87f)]
    [TrackClipType(typeof(CheckpointClip))]
    public class CheckpointTrack : TrackAsset
    {
        
    }
}