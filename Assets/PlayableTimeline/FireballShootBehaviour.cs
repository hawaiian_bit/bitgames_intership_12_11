using System;
using UnityEngine;
using UnityEngine.Playables;

namespace DefaultNamespace
{
    [Serializable]
    public class FireballShootBehaviour : PlayableBehaviour
    {
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (info.weight < 1) return;

            var fireball = (playerData as GameObject).GetComponent<Fireball>();

            if (fireball != null) 
            {
                fireball.Shoot();
            }
        }
    }
}