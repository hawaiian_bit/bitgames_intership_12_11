using System;
using UnityEngine;
using UnityEngine.Playables;

namespace DefaultNamespace
{
    [Serializable]
    public class FireballShootClip : PlayableAsset
    {        
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<FireballShootBehaviour>.Create(graph);
        }
    }
}