using UnityEngine;
using UnityEngine.Timeline;

namespace DefaultNamespace
{
    [TrackBindingType(typeof(GameObject))]
    [TrackClipType(typeof(FireballShootClip))]
    public class FireballTrack : TrackAsset
    {
        
    }
}