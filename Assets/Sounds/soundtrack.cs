﻿using System.Collections;
using UnityEngine;

public class soundtrack : MonoBehaviour
{
    [SerializeField] private AudioSource[] tracks = new AudioSource[3];

    private IEnumerator Start()
    {
        while (true)
        {
            for (var i = 1; i < tracks.Length; i++)
            {
                do
                {
                    yield return new WaitForSeconds(tracks[i - 1].clip.length - Time.deltaTime);
                } while (tracks[i - 1].loop);

                tracks[i].Play();
            }
            
            tracks[0].Play(0);
        }
    }
}
